<?php
$VERIFY_TOKEN      = 'fenohasina';
$PAGE_ACCESS_TOKEN = 'EAAe0bq8ZBvYQBABrbTwRChZB2xwMmBEbz3ogBhizsSEoO3BJqbu19FXaHorURGL0G8TSxK9FHYsX8fH0fKfZCBOZAMPvKc9uW4goiU1L1dKQiEIfSJgxQEYZAZBh1fS6AuVAQPQwYIqpChTwUdMDVH2ZAlMV1qsGrEEiA6iFtAgqdpeIRkuLATB';
$verify_token = $_REQUEST['hub_verify_token'];
if ($verify_token === $VERIFY_TOKEN) {
    //If the Verify token matches, return the challenge, which is defined by FB
    echo $challenge;
} else {
    
    $input = json_decode(file_get_contents('php://input'), true);
    // Get the Senders Page Scoped ID
    $sender = $input['entry'][0]['messaging'][0]['sender']['id'];
    // Get the message text sent
    $message = $input['entry'][0]['messaging'][0]['message']['text'];
    if (!empty($message)) {
        if ($message == "2") {
            send_image_message($sender, 'https://i.imgur.com/nOiJbXp.jpg', $PAGE_ACCESS_TOKEN);
        } 
        
        
        elseif ($message == "hi") {
          $WelcomeMessage = "Bonjour! on vous remercie de l'intention que vous portez sur notre institution. Envoyez 1 Pour voir les cours. 2 Pour savoir ou se trouve ELI. 3 Pou voir la prochaine rentée";
            send_text_message($sender, $WelcomeMessage, $PAGE_ACCESS_TOKEN);
        } 
        
        
        elseif ($message == "1") {
          $course = "Nous avons divers cours: Envoyez FR pour l'Anglais/Français IT pour l'Italien/Allemand/Espagnol. CH pour le Chinois. SAM pour le cours spécial Samedi. Et LEAD pour le cours de leadership";
            send_text_message($sender, $course, $PAGE_ACCESS_TOKEN);
        } 
        
        
        elseif($message == 'FR'){
          $français = "Pour les deux langues : Niveau1 => des textes... Niveau2 => Des textes...  ....";
           send_text_message($sender, $français, $PAGE_ACCESS_TOKEN);
        } 
        
        
        elseif($message == 'IT'){
            $italien = "Pour les 3 langues : Niveau1 => Des texted ... Niveau2 => Des textes ... ...";
             send_text_message($sender, $italien, $PAGE_ACCESS_TOKEN);
        } 
        
        
        else {
            send_text_message($sender, $message, $PAGE_ACCESS_TOKEN);
        }
    }
}

function send_message($access_token, $payload)
{
    // Send/Recieve API
    $url = 'https://graph.facebook.com/v2.6/me/messages?access_token=' . $access_token;
    // Initiate the curl
    $ch = curl_init($url);
    // Set the curl to POST
    curl_setopt($ch, CURLOPT_POST, 1);
    // Add the json payload
    curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
    // Set the header type to application/json
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    // SSL Settings
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    // Send the request
    $result = curl_exec($ch);
    //Return the result
    return $result;
}
function build_text_message_payload($sender, $message)
{
    // Build the json payload data
    $jsonData = '{
        "recipient":{
            "id":"' . $sender . '"
        },
        "message":{
            "text": "' . $message . '"
        }
    }';
    return $jsonData;
}
function send_text_message($sender, $message, $access_token)
{
    $jsonData = build_text_message_payload($sender, $message);
    $result   = send_message($access_token, $jsonData);
    return $result;
}

function build_plan_image($sender, $image_url)
{
    // Build the json payload data
    $jsonData = '{
        "recipient":{
            "id":"' . $sender . '"
        },
        "message":{
            "text": "",
            "attachment":{
              "type":"image",
              "payload":{
                "url": "' . $image_url . '"
              }
            }
        }
    }';
    return $jsonData;
}
function send_image_message($sender, $image_url, $access_token)
{
    $jsonData = build_plan_image($sender, $image_url);
    $result   = send_message($access_token, $jsonData);
    return $result;
}